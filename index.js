/* s20 Activity:

>> In the S20 folder, create an activity folder, an index.html file inside of it and link the index.js file.

>> Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.

a. 
	>> Create a variable number that will store the value of the number provided by the user via the prompt.

	>> Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration. 

	>> Create a condition that if the current value is less than or equal to 50, stop the loop. (use break statement)
	
	>> Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop. 
		>> use modulo operator and strict equality to check divisibility
		>> use continue statement
	
	>> Create another condition that if the current value is divisible by 5, print the number.

		>> use modulo operator and strict equality to check divisibility

b. Stretch Goals

	>> Create a variable that will contain the string supercalifragilisticexpialidocious.

	>> Create another variable with an emtpy string as a value that will store the consonants from the string.

	>> Create for Loop that will iterate through the individual letters of the string based on it’s length.

	>> Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.

	>> Create an else statement that will add the letter to the second variable.
		>> use addition assignment operator
		>> use the concept of index to add the specific letter in the second variable

	>> After the loop is complete, print the filtered string without the vowels
 */

let numberInput = Number(prompt('Enter a number'));

for (let i = numberInput; i >= 0; i--) {
    if (i <= 50) {
        console.log('The current value is at ' + i + '. Terminating the loop');
        break;
    }

    if (i % 10 === 0) {
        console.log(
            i +
            ' is being skipped and will continue to the next iteration of the loop\n'
        );
        continue;
    }
    if (i % 5 === 0) {
        console.log(i);
    }
}

//b. Solution 1
const words = 'supercalifragilisticexpialidocious'.toLowerCase();
let newWords = '';

for (let i = 0; i < words.length; i++) {
    if (
        words[i] == 'a' ||
        words[i] == 'e' ||
        words[i] == 'i' ||
        words[i] == 'o' ||
        words[i] == 'u'
    ) {
        continue;
    } else {
        newWords += words[i];
    }
}

console.log(newWords);

//b. Solution 2 - User input
const userInput = prompt('Enter a very long words').toLowerCase();
let newString = '';

console.log(userInput.length);

for (let i = 0; i < userInput.length; i++) {
    if (
        userInput[i] == 'a' ||
        userInput[i] == 'e' ||
        userInput[i] == 'i' ||
        userInput[i] == 'o' ||
        userInput[i] == 'u'
    ) {
        continue;
    } else {
        newString += userInput[i];
    }
}

console.log(newString);

//c. Solution 3 - Using function combined with user input and multiple invocation -----Needs to be debugged
// function removingCons(word) {
//     let lowerCase = word.toLowerCase();
//     let newStringWords = '';

//     for (let i = 0; i < lowerCase.length; i++) {
//         if (
//             lowerCase[i] == 'a' ||
//             lowerCase[i] == 'e' ||
//             lowerCase[i] == 'i' ||
//             lowerCase[i] == 'o' ||
//             lowerCase[i] == 'u'
//         ) {
//             continue;
//         } else {
//             newStringWords += lowerCase[i];
//         }
//     }

//     return newStringWords;
// }

// let a = removingCons('supercalifragilisticexpialidocious');
// let b = removingCons('Pseudopseudohypoparathyroidism');
// let c = removingCons('Floccinaucinihilipilification');
// let d = removingCons(prompt('Enter a very long words'));

// console.log(a);
// console.log(b);
// console.log(c);
// console.log(d);